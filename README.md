# Introduction
This is an image file repository for testing HSAemu.

# Extract the cpio file
* `./cpioDecompress.sh`

# Compress the rootfs file system to cpio file
* `./cpioBuild.sh`

# Run binary search benchmark
1. In guest OS, enter `test_set` directory.
2. Execute `./binarysearch_arm --load BinarySearch_Kernels.bin`

# Profiling In Guest OS
* run `./profile <TARGET PROGRAM>` to profile a program
* run `./profile "<TARGET PROGRAM> <ARG1> [ARGS...]"` with input arguments to profile a program with VPMU.


